# b5_classifier_microservice


## Installation
```bash
pip install -r requirements.txt
```

## Training
Run this at least once before deploying. This creates models for the classifier. 
```bash
python big_five_classifier/train_classifier.py
```


## Deployment
```bash
python server.py
```


## Usage

#### `POST`: `/api/v1/b5_processor`


```python
{
  "sentence": "Anxious, my mind is clouded as I lay down on my stomach, arms on my pillow across each other. Facing sideways, my ears burried in my arms; I can hear my heart beat, 'that's good,' I said. I spent the night, gazing upon the street lights through the slip of the curtains, as tears runneth on my cheeks, down towards thy pillow of sorrows. Letting the ravaging waves of anxiety drown me. My pillow is soaking wet, 'not again,' it must've said. For how many sleepless nights have I spent crying.  But nevermind, maybe I'm just tired. ",
  "key": "12345"
}
```

Result:
```python
{
  "extraversion": 1,
  "neuroticism": 1,
  "agreeableness": 1,
  "conscientiousness": 0,
  "openness": 1
}
```