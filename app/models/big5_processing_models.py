from typing import List, Dict
from pydantic import BaseModel, Field

class BigFiveProcessingRequest(BaseModel):
    sentence: List[str] = Field(description="Sentences to analyze", example=[
                                "I'm having a good day today", "Things are simply horrible"])
    key: bytes = Field(description="Blake2b hexdigest encoded with the Server's Secret Key",
                       example="45158284b578eb9df23894d4184474652f09921bb98ff11abcf3fa05a2c6dc71df9b7bddb8157f49f4f468d4365fc2fd6425802336d20a492a9e12232e99c40b")


class BigFiveProcessingResponse(BaseModel):
    predictions: Dict[str, float] = Field(description="Returns averaged Big5 Predictions for given sentences", example={
        "OPN": 73.1483699642273,
        "CON": 54.92043484373057,
        "EXT": 54.6937548222252,
        "AGR": 59.098921356952225,
        "NEU": 39.10662544854194}
    )


class BigFiveRadarGraphResponse(BaseModel):
    graph_image_path: str = Field(description="Returns a relative url to radar graph",
                                  example="/api/v1/b5/static/1604075258_5335712.png")
