from typing import List, Dict
from pydantic import BaseModel, Field

# Big5 Test Models
class QuestionResponse(BaseModel):
    Id: str = Field(description="Id of the question", example="1")
    body: str = Field(description="Body of the question",
                      example="I Am full of ideas")
    options: List[int] = Field(
        description="Available options for the question", example=[1,2,3,4,5])
    answer: str = Field(description="Answer of the question, (1-5)")


class Big5TestRequest(BaseModel):
    key: bytes = Field(
        description="Blake2b hexdigest encoded with the Server's Secret Key")
    session_key: str = Field(description="Session key of the test")


class Big5CreateTestRequest(BaseModel):
    key: bytes = Field(
        description="Blake2b hexdigest encoded with the Server's Secret Key")

class Big5AnswerQuestionRequest(BaseModel):
    key: bytes = Field(
        description="Blake2b hexdigest encoded with the Server's Secret Key")
    session_key: str = Field(description="Session key of the test")
    answer: int = Field(description="Answer for the current question")

class Big5AnswerQuestionWithIdRequest(BaseModel):
    key: bytes = Field(
        description="Blake2b hexdigest encoded with the Server's Secret Key")
    session_key: str = Field(description="Session key of the test")
    answer: int = Field(description="Answer for the current question")
    question_id: int = Field(description="Id of the question you want to answer")

class Big5GetQuestionsResponse(BaseModel):
    questions: List[QuestionResponse] = Field(
        description="Returns a list of Questions")


class Big5CreateTestResponse(BaseModel):
    session_key: str = Field(description="Session key of the created test")


class Big5GetQuestionResponse(BaseModel):
    question: QuestionResponse


class Big5EndTestResponse(BaseModel):
    results: Dict[str, int] = Field(description="Results of the test, the test scores are between 0-40", example={
        "Openness": 20,
        "Conscientiousness": 30,
        "Extroversion": 40,
        "Agreeableness": 20,
        "Neuroticism": 10}
    )
