from hashlib import blake2b
from hmac import compare_digest
import yaml
import pathlib
import os
import shutil

def load_configs(path: str):
    fs = open(path, "r")
    configs = yaml.load(fs, Loader=yaml.FullLoader)
    fs.close()
    return configs

KEY = load_configs("configs/naive_security.yaml")["security.key"]
HASH = blake2b(key=KEY.encode('utf-8')).hexdigest().encode('utf-8')

def confirm_hexdigest(hexdigest):
    return compare_digest(HASH, hexdigest)

def get_missing_models():
    model_names = ['OPN', 'CON', 'EXT', 'AGR', 'NEU']
    missing_models = []
    model_subdirectory = pathlib.Path(__file__).parent.absolute()/"big_five_classifier"/"models"
    for model in model_names:
        model_path = model_subdirectory/f'{model}_model.pkl'
        if not model_path.exists():
            print(f"{model_path} doesn't exists.")
            missing_models.append(model)
    return missing_models

def remove(path):
    if os.path.isfile(path) or os.path.islink(path):
        os.remove(path)  # remove the file
    elif os.path.isdir(path):
        shutil.rmtree(path)  # remove dir and all contains
    else:
        raise ValueError("file {} is not a file or dir.".format(path))

    print("Deleted:", path)

