from fastapi import APIRouter
from fastapi.responses import JSONResponse
from fastapi.staticfiles import StaticFiles
from functools import partial
import threading
from models.big5_processing_models import BigFiveProcessingRequest, BigFiveProcessingResponse, BigFiveRadarGraphResponse
import pathlib

# required for the pickle load to be successfull when loading BFiveClassifier
from util import BFiveClassifier, Model, train_model, load_configs, confirm_hexdigest, get_missing_models, remove


router = APIRouter()

IMAGE_PATH = pathlib.Path("./temp_figures").absolute()
IMAGE_DELETE_TIME = 30 #seconds

# Before starting the server, let's check if the models exists, if not, we train the models
missing_models = get_missing_models()

# if there are any elements in the missing models array
if missing_models:
    print(f"Missing {missing_models}, attempting to train {missing_models}")
    train_model(pathlib.Path(__file__).parent.absolute()/".."/"util"/"big_five_classifier", traits=missing_models)
# we need to create the classifier after creating the models/checking if they are there
classifier = BFiveClassifier()


@router.post("/api/v1/b5_radar", response_model= BigFiveRadarGraphResponse)
def radar_plot_endpoint(r: BigFiveProcessingRequest):
    """
        Produces a temporary link to a multi-layered radar chart. (https://en.wikipedia.org/wiki/Radar_chart)
        The radar chart displays the distribution of OCEAN scores. The image exists on the server for a total of
        30 seconds.
    """
    if not confirm_hexdigest(r.key):
        return JSONResponse(status_code=403, content={"description": "Key is invalid."})

    ### RadarPlot Creates an image and writes it to an output path we specify. The function returns the exact
    ### path of the fie.
    image_path = classifier.plot_radar(r.sentence, output_path= str(IMAGE_PATH) + '/' )

    ### We don't want to store our radar-charts forever. Scheduling a timed delete of the file after it's creation.
    ### The assumption is made that the requester gets the image within the image expiration time frame.
    threading.Timer(IMAGE_DELETE_TIME, partial(remove, image_path)).start()

    mounted_image_path =  f"/api/v1/b5/static/{pathlib.Path(image_path).name}"
    return BigFiveRadarGraphResponse(graph_image_path = mounted_image_path)

@router.post("/api/v1/b5_processor", response_model=BigFiveProcessingResponse)
def percentages_endpoint(r: BigFiveProcessingRequest):
    """
    Given a list of sentences, returns the average of OCEAN scores of all the sentences in the list. To process a single
    sentence include an array with a single entry.

    To learn more about the big-five personality index, read the wiki:
    https://en.wikipedia.org/wiki/Big_Five_personality_traits

    """
    if not confirm_hexdigest(r.key):
        return JSONResponse(status_code=403, content={"description": "Key is invalid."})
    return BigFiveProcessingResponse(predictions = classifier.classify(r.sentence))
