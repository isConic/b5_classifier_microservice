from fastapi import APIRouter
from fastapi.responses import JSONResponse
from util import Big5Test, confirm_hexdigest
from models.big5_test_models import QuestionResponse, Big5CreateTestRequest, Big5CreateTestResponse, Big5TestRequest, Big5GetQuestionsResponse, Big5AnswerQuestionRequest, Big5EndTestResponse, Big5AnswerQuestionWithIdRequest

import uuid

router = APIRouter()

tests = {}

common_responses = {
    404: {"description": "Given test with the session_key couldn't be found"},
    403: {"description": "Key is invalid."}
}


@router.post("/api/v1/create_test/", response_model=Big5CreateTestResponse, 
responses={
    403: {"description": "Key is invalid."}
})
def create_test_endpoint(r: Big5CreateTestRequest):
    """
        Creates a Big5 Test in the server and returns the session key
    """
    if not confirm_hexdigest(r.key):
        return JSONResponse(status_code=403, content={"description": "Key is invalid."})
        
    test_id = str(uuid.uuid1())
    tests[test_id] = Big5Test(test_id)

    return Big5CreateTestResponse(session_key=test_id)


@router.get("/api/v1/get_questions/", response_model=Big5GetQuestionsResponse, responses=common_responses)
def get_test_endpoint(r: Big5TestRequest):
    """
        Returns the given session_key's test's questions and answers 
    """
    if not confirm_hexdigest(r.key):
        return JSONResponse(status_code=403, content={"description": "Key is invalid."})

    test = tests.get(r.session_key, None)
    if test:
        questions = []
        for q in test.questions.values():
            questions.append(QuestionResponse(**q.__dict__))
        return Big5GetQuestionsResponse(questions=questions)
    else:
        return JSONResponse(status_code=404, content={"description": "Given test with the session_key couldn't be found"})

@router.post("/api/v1/answer_question/", responses={ **common_responses, 
    400: {"description": "Given answer is not valid"},
    200: {"description": "Question is answered successfully"}
})
def answer_question_endpoint(r: Big5AnswerQuestionRequest):
    """
        Answers the current question on the test
    """
    if not confirm_hexdigest(r.key):
        return JSONResponse(status_code=403, content={"description": "Key is invalid."})

    test = tests.get(r.session_key, None)
    if test:
        try:
            test.answer_question(r.answer)
            return JSONResponse(status_code=200, content={"description": "Question is answered successfully"})
        except ValueError as e:
            return JSONResponse(status_code=400, content={"description" : str(e)})
    else:
        return JSONResponse(status_code=404, content={"description": "Given test with the session_key couldn't be found"})


@router.get("/api/v1/get_current_question/", response_model=QuestionResponse,
responses={
    **common_responses,
    400: {"description": "There are no current question"}
})
def get_current_question_endpoint(r: Big5TestRequest):
    """
        Gets the current question in the test
    """
    if not confirm_hexdigest(r.key):
        return JSONResponse(status_code=403, content={"description": "Key is invalid."})

    test = tests.get(r.session_key, None)
    if test:
        current_q = test.get_current_question()
        if current_q:
            return QuestionResponse(**current_q.__dict__)
        else:
            return JSONResponse(status_code=400, content={"description" : "There are no current question"})
    else:
        return JSONResponse(status_code=404, content={"description": "Given test with the session_key couldn't be found"})

@router.get("/api/v1/get_next_question/", response_model=QuestionResponse, 
responses={
    **common_responses,
    404: {"description": "Given test with the session_key couldn't be found"}
})
def get_next_question(r: Big5TestRequest):
    """
        Gets the next question in the test
    """
    if not confirm_hexdigest(r.key):
        return JSONResponse(status_code=403, content={"description": "Key is invalid."})

    test = tests.get(r.session_key, None)
    if test:
        next_q = test.get_next_question()
        if next_q:
            return QuestionResponse(**next_q.__dict__)
        else:
            return JSONResponse(status_code=400, content={"description" : "There are no more questions left"})
    else:
        return JSONResponse(status_code=404, content={"description": "Given test with the session_key couldn't be found"})


@router.get("/api/v1/get_previous_question/", response_model=QuestionResponse, 
responses={
    **common_responses,
    400: {"description": "There are no more questions left"}
})
def get_previous_question(r: Big5TestRequest):
    """
        Gets the previous question in the test
    """
    if not confirm_hexdigest(r.key):
        return JSONResponse(status_code=403, content={"description": "Key is invalid."})

    test = tests.get(r.session_key, None)
    if test:
        prev_q = test.get_previous_question()
        if prev_q:
            return QuestionResponse(**prev_q.__dict__)
        else:
            return JSONResponse(status_code=400, content={"description" : "There are no more questions left"})
    else:
        return JSONResponse(status_code=404, content={"description": "Given test with the session_key couldn't be found"})


@router.post("/api/v1/end_test/", response_model=Big5EndTestResponse,
responses={
    **common_responses,
    400: {"description":"There are unanswered questions"}
})
def end_test_endpoint(r: Big5TestRequest):
    """
        Ends the test and returns the results
        or returns the unanswered question ids
    """
    if not confirm_hexdigest(r.key):
        return JSONResponse(status_code=403, content={"description": "Key is invalid."})

    test = tests.get(r.session_key, None)
    if test:
        end_test = test.end_test()
        # if there are errors in the returned dictionary
        if end_test.get("error", ""):
            return JSONResponse(status_code=400, content={"description": end_test})
        # if no errors, then map the dict to response
        tests.pop(r.session_key)
        return Big5EndTestResponse(results=end_test)
    else:
        return JSONResponse(status_code=404, content={"description": "Given test with the session_key couldn't be found"})

@router.post("/api/v1/answer_question_with_id/",
responses={
    **common_responses,
    400: {"description": "Answer is invalid"},
})
def answer_question_with_id_endpoint(r: Big5AnswerQuestionWithIdRequest):
    """
        Answers the question with the given question_id and sets the current_question to answered question
    """
    if not confirm_hexdigest(r.key):
        return JSONResponse(status_code=403, content={"description": "Key is invalid."})

    test = tests.get(r.session_key, None)
    if test:
        try:
            test.answer_specific_question(answer=r.answer, Id=r.question_id)
            return JSONResponse(status_code=200, content={"description": f"Question with Id {r.question_id} is answered successfully"})
        except ValueError as e:
            return JSONResponse(status_code=400, content={"description": str(e)})        
    else:
        return JSONResponse(status_code=404, content={"description": "Given test with the session_key couldn't be found"})