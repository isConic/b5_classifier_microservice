from .big5_prediction_router import router as big5_prediction_router
from .big5_test_router import router as big5_test_router
from .static_router import static_router