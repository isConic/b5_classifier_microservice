from fastapi.staticfiles import StaticFiles
from starlette.routing import Router
import pathlib

IMAGE_PATH = pathlib.Path("./temp_figures").absolute()

static_router = Router()
static_router.mount("/", StaticFiles(directory=str(IMAGE_PATH)), name="static")