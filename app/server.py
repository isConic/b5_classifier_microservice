import os
import pathlib
import threading
import uvicorn
from functools import partial
from fastapi import FastAPI, status, HTTPException
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware

# required for the pickle load to be successfull when loading BFiveClassifier
from util import Model, load_configs

## routers
from routers import big5_prediction_router, big5_test_router, static_router


api_app = FastAPI(title="Big5 Microservice")

#Add CORS
api_app.add_middleware(CORSMiddleware,
	allow_origins = ["*"],
	allow_methods = ["*"],
	allow_headers = ["*"],
	allow_credentials = True)


api_app.mount("/api/v1/b5/static", static_router)
api_app.include_router(big5_prediction_router, tags=["Big5 Prediction"])
api_app.include_router(big5_test_router, tags=["Big5 Test"])


if __name__ == "__main__":
    ##### ##### Load Server Configs ##### ######
    server_configs = load_configs("configs/server_config.yaml")
    HOST = server_configs["server.host"]
    PORT = server_configs["server.port"]

    ##### ##### Load SSL Configs    ##### ######
    ssl_configs = load_configs("configs/ssl_config.yaml")
    ssl_enabled = ssl_configs["ssl.enabled"]
    key_file = ssl_configs["ssl.key_path"] if ssl_enabled else None
    cert_file = ssl_configs["ssl.cert_path"] if ssl_enabled else None

    # Start Server
    uvicorn.run(api_app,
                port=PORT,
                host=HOST,
                ssl_keyfile=key_file,
                ssl_certfile=cert_file,
                )
